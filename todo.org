#+SEQ_TODO: TODO(t) IN-PROGRESS(i) DONE(d)
#+STARTUP: showall

* Todos
** TODO Fix debugger view for uneven PC
Don't assume program counter is always even. Currently the "->" arrow is not
shown when PC is uneven.
:LOGBOOK:
- Added: [2020-10-13 Tue 21:35]
:END:
