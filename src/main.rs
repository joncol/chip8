extern crate sdl2;

use chip8::{audio, KeyState, MachineState, RunningState};
use game_loop::game_loop;
use sdl2::audio::AudioDevice;
use sdl2::event::Event as SdlEvent;
use sdl2::keyboard::{Keycode, Mod};
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::render::BlendMode;
use sdl2::render::TextureQuery;
use sdl2::render::WindowCanvas;
use sdl2::ttf::Font;
use std::time::Instant;

mod chip8;

const SCALE: i32 = 16;
const WINDOW_WIDTH: i32 = chip8::SCREEN_WIDTH * SCALE;
const WINDOW_HEIGHT: i32 = chip8::SCREEN_HEIGHT * SCALE;
const PADDING: i32 = 8;

const DEBUG_CONSOLE_WIDTH: i32 = WINDOW_WIDTH / 6;
const DEBUG_VIEW_COUNT: i32 = 3;

enum Event {
    KeyDown(u8),
    KeyUp(u8),
    ToggleConsole,
    CycleDebugView(i32),
    StepExecute,
    Quit,
    Other,
}

struct Game<CB: sdl2::audio::AudioCallback> {
    pub machine_state: MachineState,
    pub canvas: WindowCanvas,
    pub audio_device: AudioDevice<CB>,
    pub clock: Instant,
    pub is_debugging: bool,
    pub debugger_visible: bool,
    pub debug_view: i32,
}

pub fn main() -> Result<(), String> {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let audio_subsystem = sdl_context.audio().unwrap();
    let audio_device = audio::init_device(&audio_subsystem);

    let ttf_context = sdl2::ttf::init().map_err(|e| e.to_string())?;
    let font = ttf_context.load_font("fonts/FiraCode-Medium.ttf", 12)?;

    let window = video_subsystem
        .window("rust-sdl2 demo", WINDOW_WIDTH as u32, WINDOW_HEIGHT as u32)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_blend_mode(BlendMode::Blend);
    canvas.set_scale(SCALE as f32, SCALE as f32).unwrap();

    let mut machine_state = MachineState::new();
    // machine_state.randomize_screen();
    machine_state.load_program(
        // "resources/roms/Breakout [Carmelo Cortez, 1979].ch8",
        // "resources/roms/Chip8 Picture.ch8"
        // "resources/roms/Delay Timer Test [Matthew Mikolay, 2010].ch8",
        // "resources/roms/Keypad Test [Hap, 2006].ch8",
        // "resources/roms/Maze (alt) [David Winter, 199x].ch8"
        // "resources/roms/Maze [David Winter, 199x].ch8"
        // "resources/roms/Particle Demo [zeroZshadow, 2008].ch8",
        // "resources/roms/Pong (alt).ch8",
        // "resources/roms/Sirpinski [Sergey Naydenov, 2010].ch8"
        "resources/roms/Space Invaders [David Winter].ch8",
        // "resources/roms/Stars [Sergey Naydenov, 2010].ch8"
        // "resources/roms/Tetris [Fran Dachille, 1991].ch8",
        // "resources/roms/Trip8 Demo (2008) [Revival Studios].ch8"
        // "resources/roms/Zero Demo [zeroZshadow, 2007].ch8",
    )?;

    let game = Game {
        machine_state,
        canvas,
        audio_device,
        clock: Instant::now(),
        is_debugging: false,
        debugger_visible: true,
        debug_view: 0,
    };

    let mut event_pump = sdl_context.event_pump().unwrap();

    game_loop(
        game,
        500,
        0.1,
        |g| {
            if g.game.clock.elapsed().as_micros() > 1_000_000 / 60 {
                g.game.clock = Instant::now();

                if g.game.machine_state.delay_timer > 0 {
                    g.game.machine_state.delay_timer -= 1;
                }

                if g.game.machine_state.sound_timer > 0 {
                    g.game.machine_state.sound_timer -= 1;
                    g.game.audio_device.resume();
                } else {
                    g.game.audio_device.pause();
                }
            }

            for event in event_pump.poll_iter() {
                match handle_event(&sdl_context, event) {
                    Event::KeyDown(k) => {
                        g.game.machine_state.keys[k as usize] = KeyState::Down;
                        if let RunningState::WaitingForKeyPress(
                            register_index,
                        ) = g.game.machine_state.running_state
                        {
                            g.game
                                .machine_state
                                .set_register(register_index as usize, k);
                            g.game.machine_state.running_state =
                                RunningState::Running;
                        }
                    }
                    Event::KeyUp(k) => {
                        g.game.machine_state.keys[k as usize] = KeyState::Up;
                    }
                    Event::ToggleConsole => {
                        g.game.debugger_visible = !g.game.debugger_visible
                    }
                    Event::CycleDebugView(d) => {
                        g.game.debug_view += d;
                        g.game.debug_view =
                            g.game.debug_view.rem_euclid(DEBUG_VIEW_COUNT);
                    }
                    Event::StepExecute => {
                        if g.game.is_debugging {
                            let instr = g
                                .game
                                .machine_state
                                .current_instruction()
                                .unwrap();
                            chip8::evaluate(instr, &mut g.game.machine_state)
                                .unwrap();
                        }
                    }
                    Event::Quit => g.exit_next_iteration = true,
                    _ => (),
                }
            }
            if !g.game.is_debugging {
                if let RunningState::Running =
                    g.game.machine_state.running_state
                {
                    let instr =
                        g.game.machine_state.current_instruction().unwrap();
                    chip8::evaluate(instr, &mut g.game.machine_state).unwrap();
                }
            }
        },
        |g| {
            render_screen(&mut g.game.canvas, &g.game.machine_state).unwrap();
            g.game.canvas.set_draw_color(Color::RGB(255, 255, 255));
            if g.game.debugger_visible {
                render_debugger(
                    &mut g.game.canvas,
                    &font,
                    &g.game.machine_state,
                    g.game.debug_view,
                )
                .unwrap();
            }

            g.game.canvas.present();
        },
    );

    Ok(())
}

fn render_screen(
    canvas: &mut WindowCanvas,
    machine_state: &MachineState,
) -> Result<(), String> {
    for y in 0..chip8::SCREEN_HEIGHT {
        for x in 0..chip8::SCREEN_WIDTH / 8 {
            let byte: u8 = machine_state.screen
                [(y * chip8::SCREEN_WIDTH / 8 + x) as usize];
            for bit in (0..8).rev() {
                let color = match byte & (1 << bit) {
                    0 => Color::RGB(0, 0, 0),
                    _ => Color::RGB(255, 255, 255),
                };
                canvas.set_draw_color(color);
                canvas.draw_point(Point::new(
                    (x * 8 + 7 - bit) as i32,
                    y as i32,
                ))?;
            }
        }
    }
    Ok(())
}

fn handle_event(sdl_context: &sdl2::Sdl, e: SdlEvent) -> Event {
    let k = sdl_context.keyboard();
    match e {
        SdlEvent::KeyDown {
            keycode: Some(Keycode::Num1),
            repeat: false,
            ..
        } => Event::KeyDown(1),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Num2),
            repeat: false,
            ..
        } => Event::KeyDown(2),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Num3),
            repeat: false,
            ..
        } => Event::KeyDown(3),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Num4),
            repeat: false,
            ..
        } => Event::KeyDown(0xc),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Q),
            repeat: false,
            ..
        } => Event::KeyDown(4),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::W),
            repeat: false,
            ..
        } => Event::KeyDown(5),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::F),
            repeat: false,
            ..
        } => Event::KeyDown(6),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::G),
            repeat: false,
            ..
        } => Event::KeyDown(0xd),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::A),
            repeat: false,
            ..
        } => Event::KeyDown(7),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::R),
            repeat: false,
            ..
        } => Event::KeyDown(8),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::S),
            repeat: false,
            ..
        } => Event::KeyDown(9),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::T),
            repeat: false,
            ..
        } => Event::KeyDown(0xe),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Z),
            repeat: false,
            ..
        } => Event::KeyDown(0xa),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::X),
            repeat: false,
            ..
        } => Event::KeyDown(0),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::C),
            repeat: false,
            ..
        } => Event::KeyDown(0xb),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::V),
            repeat: false,
            ..
        } => Event::KeyDown(0xf),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::Num1),
            ..
        } => Event::KeyUp(1),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::Num2),
            ..
        } => Event::KeyUp(2),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::Num3),
            ..
        } => Event::KeyUp(3),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::Num4),
            ..
        } => Event::KeyUp(0xc),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::Q),
            ..
        } => Event::KeyUp(4),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::W),
            ..
        } => Event::KeyUp(5),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::F),
            ..
        } => Event::KeyUp(6),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::G),
            ..
        } => Event::KeyUp(0xd),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::A),
            ..
        } => Event::KeyUp(7),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::R),
            ..
        } => Event::KeyUp(8),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::S),
            ..
        } => Event::KeyUp(9),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::T),
            ..
        } => Event::KeyUp(0xe),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::Z),
            ..
        } => Event::KeyUp(0xa),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::X),
            ..
        } => Event::KeyUp(0),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::C),
            ..
        } => Event::KeyUp(0xb),

        SdlEvent::KeyUp {
            keycode: Some(Keycode::V),
            ..
        } => Event::KeyUp(0xf),

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Backquote),
            ..
        } => Event::ToggleConsole,

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Tab),
            ..
        } => {
            if (k.mod_state() & Mod::LSHIFTMOD) != Mod::NOMOD {
                Event::CycleDebugView(-1)
            } else {
                Event::CycleDebugView(1)
            }
        }

        SdlEvent::KeyDown {
            keycode: Some(Keycode::Space),
            ..
        } => Event::StepExecute,

        SdlEvent::Quit { .. }
        | SdlEvent::KeyDown {
            keycode: Some(Keycode::Escape),
            ..
        } => Event::Quit,

        _ => Event::Other,
    }
}

fn render_debugger(
    canvas: &mut WindowCanvas,
    font: &Font,
    machine_state: &MachineState,
    debug_view: i32,
) -> Result<(), String> {
    let code_x: i32 = WINDOW_WIDTH - 2 * DEBUG_CONSOLE_WIDTH;
    let regs_x: i32 = WINDOW_WIDTH - DEBUG_CONSOLE_WIDTH;

    let (sx, sy) = canvas.scale();
    canvas.set_scale(1.0, 1.0).unwrap();

    canvas.set_draw_color(Color::RGBA(63, 63, 63, 192));
    canvas.fill_rect(Rect::new(
        code_x as i32,
        0,
        (DEBUG_CONSOLE_WIDTH * 2) as u32,
        WINDOW_HEIGHT as u32,
    ))?;

    let text = "   Code\n   \
                   ----------\n"
        .to_string();

    render_text(
        canvas,
        &font,
        Color::RGBA(255, 255, 0, 255),
        Point::new(code_x, 0),
        DEBUG_CONSOLE_WIDTH,
        0,
        &text,
    )?;

    let pc_offset = ((machine_state.pc - 0x200) / 2) as i32;
    let mut text = "".to_string();
    for (i, instr) in machine_state.memory[(chip8::PROGRAM_OFFSET) as usize
        ..(chip8::PROGRAM_OFFSET) as usize + machine_state.code_size]
        .chunks(2)
        .enumerate()
    {
        let pc = (i * 2) as u16 + chip8::PROGRAM_OFFSET as u16;
        text += &format!(
            "{:3}{:04x}: {}\n",
            if pc == machine_state.pc { "->" } else { "" },
            pc,
            hex::encode(instr)
        );
    }

    let lines_visible_count =
        (WINDOW_HEIGHT - PADDING * 2) as i32 / font.height();
    let y_offset = if pc_offset >= lines_visible_count - 2 - 3 {
        (pc_offset - (lines_visible_count - 5)) * font.height()
    } else {
        0
    };

    render_text(
        canvas,
        &font,
        Color::RGBA(255, 255, 0, 255),
        Point::new(code_x, font.height() * 2),
        DEBUG_CONSOLE_WIDTH,
        y_offset,
        &text,
    )?;

    match debug_view {
        0 => {
            let mut text = "Registers\n\
                            ---------\n"
                .to_string();
            for i in 0..16 {
                text.push_str(&format!(
                    "V{:X}: {} (0x{:02x})\n",
                    i, machine_state.regs[i], machine_state.regs[i]
                ));
            }
            text.push_str(&format!("\nI: 0x{:04x}\n", machine_state.ri));
            render_text(
                canvas,
                &font,
                Color::RGBA(127, 255, 255, 255),
                Point::new(regs_x, 0),
                DEBUG_CONSOLE_WIDTH,
                0,
                &text,
            )?;
        }
        1 => {
            let mut text = "Special Regs\n\
                            ------------\n"
                .to_string();
            text.push_str(&format!("pc: 0x{:04x}\n", machine_state.pc));
            text.push_str(&format!(
                "dt: 0x{:02x}\n",
                machine_state.delay_timer
            ));
            text.push_str(&format!(
                "st: 0x{:02x}\n",
                machine_state.sound_timer
            ));
            render_text(
                canvas,
                &font,
                Color::RGBA(127, 255, 255, 255),
                Point::new(regs_x, 0),
                DEBUG_CONSOLE_WIDTH,
                0,
                &text,
            )?;
        }
        2 => {
            let mut text = "Stack\n-----\n".to_string();
            if machine_state.stack.is_empty() {
                text.push_str("empty");
            } else {
                for i in (0..machine_state.stack.len()).rev() {
                    text.push_str(&format!(
                        "{:3}{} (0x{:02x})\n",
                        if i == (machine_state.stack.len() - 1) as usize {
                            "->"
                        } else {
                            ""
                        },
                        machine_state.stack[i],
                        machine_state.stack[i]
                    ));
                }
            }
            render_text(
                canvas,
                &font,
                Color::RGBA(127, 255, 255, 255),
                Point::new(regs_x, 0),
                DEBUG_CONSOLE_WIDTH,
                0,
                &text,
            )?;
        }
        _ => {}
    }

    // TODO: Reset scale also when something fails above.
    canvas.set_scale(sx, sy).unwrap();

    Ok(())
}

fn render_text(
    canvas: &mut WindowCanvas,
    font: &Font,
    color: Color,
    point: Point,
    width: i32,
    y_offset: i32,
    text: &str,
) -> Result<(), String> {
    let surface = font
        .render(text)
        .blended_wrapped(color, (width - PADDING * 2) as u32)
        .map_err(|e| e.to_string())?;

    let texture_creator = canvas.texture_creator();

    let texture = texture_creator
        .create_texture_from_surface(&surface)
        .map_err(|e| e.to_string())?;

    let TextureQuery {
        width: w,
        height: h,
        ..
    } = texture.query();
    let px = point.x();
    let py = point.y();
    let target = Rect::new(
        (px + PADDING) as i32,
        (py + PADDING) as i32 - y_offset,
        w,
        h,
    );
    let clip_rect = Rect::new(
        (px + PADDING) as i32,
        (py + PADDING) as i32,
        width as u32,
        (WINDOW_HEIGHT - py - PADDING * 2) as u32,
    );
    canvas.set_clip_rect(clip_rect);
    canvas.copy(&texture, None, Some(target))?;
    canvas.set_clip_rect(None);

    Ok(())
}
