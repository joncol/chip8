pub mod audio;
pub mod eval;
pub mod machine_state;

pub use eval::evaluate;
pub use machine_state::KeyState;
pub use machine_state::MachineState;
pub use machine_state::RunningState;

pub const PROGRAM_OFFSET: u16 = 0x200;
pub const SCREEN_WIDTH: i32 = 64;
pub const SCREEN_HEIGHT: i32 = 32;
pub const SCREEN_SIZE: i32 =
    crate::chip8::SCREEN_WIDTH / 8 * crate::chip8::SCREEN_HEIGHT;
