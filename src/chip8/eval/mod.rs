use super::machine_state::{KeyState, MachineState, RunningState};
use crate::chip8;
use rand::prelude::*;
use std::num::Wrapping;
use std::ptr;

mod eval_test;

pub fn evaluate(instr: u16, ms: &mut MachineState) -> Result<(), String> {
    match instr {
        // 0nnn - SYS addr
        0x0100..=0x0fff => ms.pc = instr,

        // 00E0 - CLS
        0x00e0 => unsafe {
            let vec_ptr = ms.screen.as_mut_ptr();
            ptr::write_bytes(vec_ptr, 0, chip8::SCREEN_SIZE as usize);
        },

        // 00EE - RET
        0x00ee => {
            ms.pc = ms.stack.pop().ok_or("Stack is empty")?;
        }

        // 1nnn - JP addr
        0x1000..=0x1fff => ms.pc = instr & 0x0fff,

        // 2nnn - CALL addr
        0x2000..=0x2fff => {
            ms.stack.push(ms.pc + 2);
            ms.pc = instr & 0x0fff;
        }

        // 3xkk - SE Vx, byte
        0x3000..=0x3fff => {
            let vx = (instr & 0x0f00) >> 8;
            let value: u8 = (instr & 0x00ff) as u8;
            if ms.register(vx as usize) == value {
                ms.pc += 4;
            } else {
                ms.pc += 2;
            }
        }

        // 4xkk - SNE Vx, byte
        0x4000..=0x4fff => {
            let vx = (instr & 0x0f00) >> 8;
            let value: u8 = (instr & 0x00ff) as u8;
            if ms.register(vx as usize) != value {
                ms.pc += 4;
            } else {
                ms.pc += 2;
            }
        }

        // 5xy0 - SE Vx, Vy
        0x5000..=0x5ff0 => {
            let vx = (instr & 0x0f00) >> 8;
            let vy = (instr & 0x00f0) >> 4;
            if ms.register(vx as usize) == ms.register(vy as usize) {
                ms.pc += 4;
            } else {
                ms.pc += 2;
            }
        }

        // 6xkk - LD Vx, byte
        0x6000..=0x6fff => {
            let vx = (instr & 0x0f00) >> 8;
            let value: u8 = (instr & 0x00ff) as u8;
            ms.set_register(vx as usize, value);
        }

        // 7xkk - ADD Vx, byte
        0x7000..=0x7fff => {
            let vx: usize = ((instr & 0x0f00) >> 8) as usize;
            let value: u8 = (instr & 0x00ff) as u8;
            ms.regs[vx] += Wrapping(value);
        }

        0x8000..=0x8fff => {
            let vx = (instr & 0x0f00) >> 8;
            let vy = (instr & 0x00f0) >> 4;
            match instr & 0x0f {
                // 8xy0 - LD Vx, Vy
                0 => ms.set_register(vx as usize, ms.register(vy as usize)),

                // 8xy1 - OR Vx, Vy
                1 => ms.set_register(
                    vx as usize,
                    ms.register(vx as usize) | ms.register(vy as usize),
                ),

                // 8xy2 - AND Vx, Vy
                2 => ms.set_register(
                    vx as usize,
                    ms.register(vx as usize) & ms.register(vy as usize),
                ),

                // 8xy3 - XOR Vx, Vy
                3 => ms.set_register(
                    vx as usize,
                    ms.register(vx as usize) ^ ms.register(vy as usize),
                ),

                // 8xy4 - ADD Vx, Vy
                4 => {
                    let sum = ms.register(vx as usize) as i32
                        + ms.register(vy as usize) as i32;
                    ms.regs[vx as usize] += ms.regs[vy as usize];
                    ms.set_register(0xf, if sum > 255 { 1 } else { 0 });
                }

                // 8xy5 - SUB Vx, Vy
                5 => {
                    ms.set_register(
                        0xf,
                        if ms.register(vx as usize) > ms.register(vy as usize) {
                            1
                        } else {
                            0
                        },
                    );
                    ms.regs[vx as usize] -= ms.regs[vy as usize];
                }

                // 8xy6 - SHR Vx {, Vy}
                6 => {
                    ms.set_register(
                        0xf,
                        if (ms.register(vx as usize) & 0x01) == 0 {
                            0
                        } else {
                            1
                        },
                    );
                    ms.regs[vx as usize] >>= 1;
                }

                // 8xy7 - SUBN Vx, Vy
                7 => {
                    ms.set_register(
                        0xf,
                        if ms.register(vy as usize) > ms.register(vx as usize) {
                            1
                        } else {
                            0
                        },
                    );
                    ms.regs[vx as usize] =
                        ms.regs[vy as usize] - ms.regs[vx as usize];
                }

                // 8xyE - SHL Vx {, Vy}
                0xe => {
                    ms.set_register(
                        0xf,
                        if (ms.register(vx as usize) & 0x80) == 0 {
                            0
                        } else {
                            1
                        },
                    );
                    ms.regs[vx as usize] <<= 1;
                }

                _ => {
                    return Err(format!("Unknown instruction: 0x{:04x}", instr))
                }
            }
        }

        // 9xy0 - SNE Vx, Vy
        0x9000..=0x9ff0 => {
            let vx = (instr & 0x0f00) >> 8;
            let vy = (instr & 0x00f0) >> 4;
            if ms.register(vx as usize) != ms.register(vy as usize) {
                ms.pc += 4;
            } else {
                ms.pc += 2;
            }
        }

        // Annn - LD I, addr
        0xa000..=0xafff => ms.ri = instr & 0x0fff,

        // Bnnn - JP V0, addr
        0xb000..=0xbfff => {
            ms.pc = (instr & 0x0fff) + ms.register(0) as u16;
        }

        // Cxkk - RND Vx, byte
        0xc000..=0xcfff => {
            let vx = (instr & 0x0f00) >> 8;
            let mask = (instr & 0x00ff) as u16;
            let mut rng = rand::thread_rng();
            let r: u16 = rng.gen_range(0, 256);
            ms.set_register(vx as usize, (r & mask) as u8);
        }

        // Dxyn - DRW Vx, Vy, nibble
        0xd000..=0xdfff => {
            let vx: u8 = ms.register(((instr & 0x0f00) >> 8) as usize);
            let vy: u8 = ms.register(((instr & 0x00f0) >> 4) as usize);
            let n: u8 = (instr & 0x000f) as u8;
            let dest_x1 = (vx / 8) as i32 % (chip8::SCREEN_WIDTH / 8);
            let dest_x2 = (dest_x1 + 1) as i32 % (chip8::SCREEN_WIDTH / 8);
            let shift = vx & 0x7;
            ms.set_register(0xf, 0);
            for y in 0..n {
                let src_byte = ms.memory[(ms.ri + y as u16) as usize];
                let dest_y = (y + vy) as i32 % chip8::SCREEN_HEIGHT;
                let dest_offset1 = (dest_y as i32 * chip8::SCREEN_WIDTH / 8
                    + dest_x1) as usize;
                let pattern1 = src_byte >> shift;
                if (ms.screen[dest_offset1] & pattern1) != 0 {
                    ms.set_register(0xf, 1);
                }
                ms.screen[dest_offset1] ^= pattern1;

                if shift > 0 {
                    let dest_offset2 =
                        (dest_y * chip8::SCREEN_WIDTH / 8 + dest_x2) as usize;
                    let pattern2 = src_byte << (8 - shift);
                    if (ms.screen[dest_offset2] & pattern2) != 0 {
                        ms.set_register(0xf, 1);
                    }
                    ms.screen[dest_offset2] ^= pattern2;
                }
            }
        }

        0xe000..=0xefff => match instr & 0xff {
            // Ex9E - SKP Vx
            0x9e => {
                let vx: u8 = ms.register(((instr & 0x0f00) >> 8) as usize);
                ms.pc += if ms.keys[vx as usize] == KeyState::Down {
                    4
                } else {
                    2
                };
            }

            // ExA1 - SKNP Vx
            0xa1 => {
                let vx: u8 = ms.register(((instr & 0x0f00) >> 8) as usize);
                ms.pc += if ms.keys[vx as usize] == KeyState::Up {
                    4
                } else {
                    2
                };
            }

            _ => return Err(format!("Unknown instruction: 0x{:04x}", instr)),
        },

        0xf000..=0xffff => match instr & 0xff {
            // Fx07 - LD Vx, DT
            0x07 => {
                let vx = (instr & 0x0f00) >> 8;
                ms.set_register(vx as usize, ms.delay_timer);
            }

            // Fx0A - LD Vx, K
            0x0a => {
                let vx = (instr & 0x0f00) >> 8;
                ms.running_state = RunningState::WaitingForKeyPress(vx as u8);
            }

            // Fx15 - LD DT, Vx
            0x15 => {
                let vx: u8 = ms.register(((instr & 0x0f00) >> 8) as usize);
                ms.delay_timer = vx;
            }

            // Fx18 - LD ST, Vx
            0x18 => {
                let vx: u8 = ms.register(((instr & 0x0f00) >> 8) as usize);
                ms.sound_timer = vx;
            }

            // Fx1E - ADD I, Vx
            0x1e => {
                let vx = ms.register(((instr & 0x0f00) >> 8) as usize) as u16;
                ms.ri += vx;
            }

            // Fx29 - LD F, Vx
            0x29 => {
                let vx = ((instr & 0x0f00) >> 8) as u8;
                ms.ri = (vx * 5) as u16;
            }

            // Fx33 - LD B, Vx
            0x33 => {
                let vx = ms.register(((instr & 0x0f00) >> 8) as usize) as u8;
                let dest_addr = ms.ri as usize;
                ms.memory[dest_addr] = vx / 100;
                ms.memory[dest_addr + 1] = (vx % 100) / 10;
                ms.memory[dest_addr + 2] = vx % 10;
            }

            // Fx55 - LD [I], Vx
            0x55 => {
                let vx = (instr & 0x0f00) >> 8;
                let mut dest_addr = ms.ri as usize;
                for i in 0..=vx {
                    ms.memory[dest_addr] = ms.register(i as usize);
                    dest_addr += 1;
                }
            }

            // Fx65 - LD Vx, [I]
            0x65 => {
                let vx = (instr & 0x0f00) >> 8;
                let mut src_addr = ms.ri as usize;
                for i in 0..=vx {
                    ms.set_register(i as usize, ms.memory[src_addr]);
                    src_addr += 1;
                }
            }

            _ => return Err(format!("Unknown instruction: 0x{:04x}", instr)),
        },

        _ => return Err(format!("Unknown instruction: 0x{:04x}", instr)),
    }

    match instr {
        0x00e0
        | 0x6000..=0x6fff
        | 0x7000..=0x7fff
        | 0x8000..=0x8fff
        | 0xa000..=0xafff
        | 0xc000..=0xcfff
        | 0xd000..=0xdfff
        | 0xf000..=0xffff => {
            ms.pc += 2;
        }
        _ => {}
    }

    Ok(())
}
