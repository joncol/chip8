#[cfg(test)]
use super::evaluate;

#[cfg(test)]
use crate::chip8::MachineState;

#[cfg(test)]
mod sys_addr_test {
    use super::*;

    #[test]
    fn sets_pc_to_a_value() {
        let mut ms = MachineState::new();
        evaluate(0x0124, &mut ms).unwrap();
        assert_eq!(0x0124, ms.pc);
    }
}

#[cfg(test)]
mod cls_test {
    use super::*;

    #[test]
    fn clears_screen() {
        let mut ms = MachineState::new();
        ms.randomize_screen();
        evaluate(0x00e0, &mut ms).unwrap();
        assert_eq!(vec![0; crate::chip8::SCREEN_SIZE], ms.screen);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x00e0, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ret_test {
    use super::*;

    #[test]
    fn sets_the_pc_to_top_of_stack() {
        let mut ms = MachineState::new();
        ms.stack.push(1024);
        evaluate(0x00ee, &mut ms).unwrap();
        assert_eq!(1024, ms.pc);
    }

    #[test]
    fn fails_when_stack_is_empty() {
        let mut ms = MachineState::new();
        assert!(evaluate(0x00ee, &mut ms).is_err());
    }
}

#[cfg(test)]
mod jp_addr_test {
    use super::*;

    #[test]
    fn sets_pc_to_a_value() {
        let mut ms = MachineState::new();
        evaluate(0x1123, &mut ms).unwrap();
        assert_eq!(0x0123, ms.pc);
    }
}

#[cfg(test)]
mod call_addr_test {
    use super::*;

    #[test]
    fn sets_pc_to_a_value() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x2124, &mut ms).unwrap();
        assert_eq!(0x0124, ms.pc);
        assert_eq!(old_pc + 2, ms.stack.pop().unwrap());
    }
}

#[cfg(test)]
mod se_vx_byte_test {
    use super::*;

    #[test]
    fn skips_next_instruction_if_register_matches_value() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        let old_pc = ms.pc;
        evaluate(0x3142, &mut ms).unwrap();
        assert_eq!(old_pc + 4, ms.pc);
    }

    #[test]
    fn doesnt_skip_next_instruction_if_register_doesnt_match_value() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        let old_pc = ms.pc;
        evaluate(0x3143, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod sne_vx_byte_test {
    use super::*;

    #[test]
    fn doesnt_skip_next_instruction_if_register_matches_value() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        let old_pc = ms.pc;
        evaluate(0x4142, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }

    #[test]
    fn skips_next_instruction_if_register_doesnt_match_value() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        let old_pc = ms.pc;
        evaluate(0x4143, &mut ms).unwrap();
        assert_eq!(old_pc + 4, ms.pc);
    }
}

#[cfg(test)]
mod se_vx_vy_test {
    use super::*;

    #[test]
    fn skips_next_instruction_if_registers_match() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        ms.set_register(2, 0x42);
        let old_pc = ms.pc;
        evaluate(0x5120, &mut ms).unwrap();
        assert_eq!(old_pc + 4, ms.pc);
    }

    #[test]
    fn doesnt_skip_next_instruction_if_registers_dont_match() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        ms.set_register(2, 0x43);
        let old_pc = ms.pc;
        evaluate(0x5120, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_vx_byte_test {
    use super::*;

    #[test]
    fn sets_a_register_to_a_value() {
        let mut ms = MachineState::new();
        evaluate(0x61ab, &mut ms).unwrap();
        assert_eq!(0xab, ms.register(1));
    }

    #[test]
    fn sets_another_register_to_a_value() {
        let mut ms = MachineState::new();
        evaluate(0x6fee, &mut ms).unwrap();
        assert_eq!(0xee, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x61ab, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod add_vx_byte_test {
    use super::*;

    #[test]
    fn increments_a_register_by_a_value() {
        let mut ms = MachineState::new();
        ms.set_register(1, 2);
        evaluate(0x7140, &mut ms).unwrap();
        assert_eq!(0x42, ms.register(1));
    }

    #[test]
    fn handles_overflow_silently() {
        let mut ms = MachineState::new();
        ms.set_register(5, 0xff);
        evaluate(0x7503, &mut ms).unwrap();
        assert_eq!(0x02, ms.register(5));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x7104, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_vx_vy_test {
    use super::*;

    #[test]
    fn sets_a_register_to_the_value_of_another_register() {
        let mut ms = MachineState::new();
        ms.set_register(2, 0xff);
        evaluate(0x8120, &mut ms).unwrap();
        assert_eq!(0xff, ms.register(1));
        assert_eq!(0xff, ms.register(2));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8120, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod or_vx_vy_test {
    use super::*;

    #[test]
    fn calculates_bitwise_or_of_a_register_with_another_register() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0b1010);
        ms.set_register(2, 0b0111);
        evaluate(0x8121, &mut ms).unwrap();
        assert_eq!(0b1111, ms.register(1));
        assert_eq!(0b0111, ms.register(2));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8121, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod and_vx_vy_test {
    use super::*;

    #[test]
    fn calculates_bitwise_and_of_a_register_with_another_register() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0b1010);
        ms.set_register(2, 0b0111);
        evaluate(0x8122, &mut ms).unwrap();
        assert_eq!(0b0010, ms.register(1));
        assert_eq!(0b0111, ms.register(2));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8122, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod xor_vx_vy_test {
    use super::*;

    #[test]
    fn calculates_bitwise_xor_of_a_register_with_another_register() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0b1010);
        ms.set_register(2, 0b0111);
        evaluate(0x8123, &mut ms).unwrap();
        assert_eq!(0b1101, ms.register(1));
        assert_eq!(0b0111, ms.register(2));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8123, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod add_vx_vy_test {
    use super::*;

    #[test]
    fn calculates_sum_of_two_registers_without_carry() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x40);
        ms.set_register(2, 0x02);
        evaluate(0x8124, &mut ms).unwrap();
        assert_eq!(0x42, ms.register(1));
        assert_eq!(0x02, ms.register(2));
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn wraps_and_sets_carry_when_sum_overflows() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0xff);
        ms.set_register(2, 0x02);
        evaluate(0x8124, &mut ms).unwrap();
        assert_eq!(0x01, ms.register(1));
        assert_eq!(0x02, ms.register(2));
        assert_eq!(1, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8124, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod sub_vx_vy_test {
    use super::*;

    #[test]
    fn calculates_difference_of_two_registers() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x44);
        ms.set_register(2, 0x02);
        evaluate(0x8125, &mut ms).unwrap();
        assert_eq!(0x42, ms.register(1));
        assert_eq!(0x02, ms.register(2));
        assert_eq!(1, ms.register(0xf));
    }

    #[test]
    fn wraps_and_sets_carry_when_difference_underflows() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x01);
        ms.set_register(2, 0x02);
        evaluate(0x8125, &mut ms).unwrap();
        assert_eq!(0xff, ms.register(1));
        assert_eq!(0x02, ms.register(2));
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8125, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod shr_vx_test {
    use super::*;

    #[test]
    fn shifts_a_register_right() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        evaluate(0x8106, &mut ms).unwrap();
        assert_eq!(0x21, ms.register(1));
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn sets_carry_when_bit_0_is_set() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x43);
        evaluate(0x8106, &mut ms).unwrap();
        assert_eq!(0x21, ms.register(1));
        assert_eq!(1, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8106, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod subn_vx_vy_test {
    use super::*;

    #[test]
    fn calculates_difference_of_two_registers() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x02);
        ms.set_register(2, 0x44);
        evaluate(0x8127, &mut ms).unwrap();
        assert_eq!(0x42, ms.register(1));
        assert_eq!(0x44, ms.register(2));
        assert_eq!(1, ms.register(0xf));
    }

    #[test]
    fn wraps_and_sets_carry_when_difference_underflows() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x02);
        ms.set_register(2, 0x01);
        evaluate(0x8127, &mut ms).unwrap();
        assert_eq!(0xff, ms.register(1));
        assert_eq!(0x01, ms.register(2));
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x8127, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod shl_vx_test {
    use super::*;

    #[test]
    fn shifts_a_register_left() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x21);
        evaluate(0x810e, &mut ms).unwrap();
        assert_eq!(0x42, ms.register(1));
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn sets_carry_when_bit_7_is_set() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x81);
        evaluate(0x810e, &mut ms).unwrap();
        assert_eq!(0x02, ms.register(1));
        assert_eq!(1, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0x810e, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod sne_vx_vy_test {
    use super::*;

    #[test]
    fn skips_next_instruction_if_registers_dont_match() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        ms.set_register(2, 0x43);
        let old_pc = ms.pc;
        evaluate(0x9120, &mut ms).unwrap();
        assert_eq!(old_pc + 4, ms.pc);
    }

    #[test]
    fn doesnt_skip_next_instruction_if_registers_match() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        ms.set_register(2, 0x42);
        let old_pc = ms.pc;
        evaluate(0x9120, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_i_addr_test {
    use super::*;

    #[test]
    fn sets_register_i_to_value() {
        let mut ms = MachineState::new();
        evaluate(0xa123, &mut ms).unwrap();
        assert_eq!(0x123, ms.ri);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xa123, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod jp_v0_addr_test {
    use super::*;

    #[test]
    fn sets_pc_to_a_value_plus_register_0() {
        let mut ms = MachineState::new();
        ms.set_register(0, 0x40);
        evaluate(0xb002, &mut ms).unwrap();
        assert_eq!(0x0042, ms.pc);
    }
}

#[cfg(test)]
mod rnd_vx_byte_test {
    use super::*;

    #[test]
    fn sets_register_to_a_random_value_anded_with_value() {
        let mut ms = MachineState::new();
        ms.set_register(4, 0xff);
        evaluate(0xc40f, &mut ms).unwrap();
        assert!(ms.register(4) <= 0x0f);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xc40f, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod drw_vx_vy_nibble_test {
    use super::*;
    use crate::chip8;

    #[test]
    fn can_draw_1_byte_sprite_at_0_0() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 0);
        ms.set_register(1, 0);
        ms.memory[SRC_ADDR as usize] = 0xff;
        evaluate(0xd011, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[0] = 0xff;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn can_draw_1_byte_sprite_at_1_0() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 1);
        ms.set_register(1, 0);
        ms.memory[SRC_ADDR as usize] = 0xff;
        evaluate(0xd011, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[0] = 0x7f;
        expected_screen[1] = 0x80;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn can_draw_1_byte_sprite_at_2_0() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 2);
        ms.set_register(1, 0);
        ms.memory[SRC_ADDR as usize] = 0xff;
        evaluate(0xd011, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[0] = 0x3f;
        expected_screen[1] = 0xc0;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn can_draw_1_byte_sprite_at_0_1() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 0);
        ms.set_register(1, 1);
        ms.memory[SRC_ADDR as usize] = 0xff;
        evaluate(0xd011, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[(chip8::SCREEN_WIDTH / 8) as usize] = 0xff;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn sets_vf_if_any_pixels_are_erased() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 0);
        ms.set_register(1, 0);
        ms.memory[SRC_ADDR as u32 as usize] = 0xff;
        ms.screen[0] = 0x01;
        evaluate(0xd011, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[0] = 0xfe;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(1, ms.register(0xf));
    }

    #[test]
    fn can_draw_sprite_that_overlaps_horizontally() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 63);
        ms.set_register(1, 0);
        ms.memory[SRC_ADDR as usize] = 0xff;
        evaluate(0xd011, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[0] = 0xfe;
        expected_screen[7] = 0x01;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn can_draw_sprite_that_overlaps_vertically() {
        let mut ms = MachineState::new();
        const SRC_ADDR: u16 = 0x0400;
        ms.ri = SRC_ADDR;
        ms.set_register(0, 0);
        ms.set_register(1, 31);
        ms.memory[SRC_ADDR as usize] = 0xff;
        ms.memory[SRC_ADDR as usize + 1] = 0xff;
        evaluate(0xd012, &mut ms).unwrap();
        let mut expected_screen: Vec<u8> = vec![0; chip8::SCREEN_SIZE];
        expected_screen[0] = 0xff;
        expected_screen[(31 * (chip8::SCREEN_WIDTH / 8)) as usize] = 0xff;
        assert_eq!(expected_screen, ms.screen);
        assert_eq!(0, ms.register(0xf));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xd011, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod skp_vx_test {
    use super::*;
    use crate::chip8::KeyState;

    #[test]
    fn doesnt_skip_next_instruction_if_key_vx_is_not_pressed() {
        let mut ms = MachineState::new();
        ms.set_register(1, 10);
        ms.keys[10] = KeyState::Up;
        let old_pc = ms.pc;
        evaluate(0xe19e, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }

    #[test]
    fn skips_next_instruction_if_key_vx_is_pressed() {
        let mut ms = MachineState::new();
        ms.set_register(1, 10);
        ms.keys[10] = KeyState::Down;
        let old_pc = ms.pc;
        evaluate(0xe19e, &mut ms).unwrap();
        assert_eq!(old_pc + 4, ms.pc);
    }
}

#[cfg(test)]
mod sknp_vx_test {
    use super::*;
    use crate::chip8::KeyState;

    #[test]
    fn skips_next_instruction_if_key_vx_is_not_pressed() {
        let mut ms = MachineState::new();
        ms.set_register(1, 10);
        ms.keys[10] = KeyState::Up;
        let old_pc = ms.pc;
        evaluate(0xe1a1, &mut ms).unwrap();
        assert_eq!(old_pc + 4, ms.pc);
    }

    #[test]
    fn doesnt_skip_next_instruction_if_key_vx_is_pressed() {
        let mut ms = MachineState::new();
        ms.set_register(1, 10);
        ms.keys[10] = KeyState::Down;
        let old_pc = ms.pc;
        evaluate(0xe1a1, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_vx_dt_test {
    use super::*;

    #[test]
    fn sets_a_register_to_the_value_of_dt() {
        let mut ms = MachineState::new();
        ms.delay_timer = 0x42;
        evaluate(0xf107, &mut ms).unwrap();
        assert_eq!(0x42, ms.register(1));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf107, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_vx_k_test {
    use super::*;
    use crate::chip8::RunningState;

    #[test]
    fn blocks_and_waits_for_keypress() {
        let mut ms = MachineState::new();
        evaluate(0xf10a, &mut ms).unwrap();
        assert_eq!(RunningState::WaitingForKeyPress(1), ms.running_state);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf10a, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_dt_vx_test {
    use super::*;

    #[test]
    fn sets_delay_timer_to_the_value_of_a_register() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        evaluate(0xf115, &mut ms).unwrap();
        assert_eq!(0x42, ms.delay_timer);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf115, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_st_vx_test {
    use super::*;

    #[test]
    fn sets_sound_timer_to_the_value_of_a_register() {
        let mut ms = MachineState::new();
        ms.set_register(1, 0x42);
        evaluate(0xf118, &mut ms).unwrap();
        assert_eq!(0x42, ms.sound_timer);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf118, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod add_i_vx_test {
    use super::*;

    #[test]
    fn adds_the_value_of_a_register_to_register_i() {
        let mut ms = MachineState::new();
        ms.ri = 0x40;
        ms.set_register(1, 0x02);
        evaluate(0xf11e, &mut ms).unwrap();
        assert_eq!(0x42, ms.ri);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf11e, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_f_vx_test {
    use super::*;

    #[test]
    fn sets_i_to_the_location_of_sprite_for_digit_vx() {
        let mut ms = MachineState::new();
        ms.set_register(1, 1);
        const DEST_ADDR: usize = 0x400;
        ms.ri = DEST_ADDR as u16;
        evaluate(0xf129, &mut ms).unwrap();
        assert_eq!(5, ms.ri);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf129, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_b_vx_test {
    use super::*;

    #[test]
    fn stores_the_digits_of_the_value_of_a_register_into_memory() {
        let mut ms = MachineState::new();
        ms.set_register(1, 123);
        const DEST_ADDR: usize = 0x400;
        ms.ri = DEST_ADDR as u16;
        evaluate(0xf133, &mut ms).unwrap();
        assert_eq!(1, ms.memory[DEST_ADDR]);
        assert_eq!(2, ms.memory[DEST_ADDR + 1]);
        assert_eq!(3, ms.memory[DEST_ADDR + 2]);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf133, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_i_vx_test {
    use super::*;

    #[test]
    fn stores_the_values_of_some_registers_into_memory() {
        let mut ms = MachineState::new();
        const DEST_ADDR: usize = 0x400;
        ms.ri = DEST_ADDR as u16;
        ms.set_register(0, 1);
        ms.set_register(1, 2);
        ms.set_register(2, 4);
        ms.set_register(3, 8);
        evaluate(0xf355, &mut ms).unwrap();
        assert_eq!(1, ms.memory[DEST_ADDR]);
        assert_eq!(2, ms.memory[DEST_ADDR + 1]);
        assert_eq!(4, ms.memory[DEST_ADDR + 2]);
        assert_eq!(8, ms.memory[DEST_ADDR + 3]);
        assert_eq!(0, ms.memory[DEST_ADDR + 4]);
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf155, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}

#[cfg(test)]
mod ld_vx_i_test {
    use super::*;

    #[test]
    fn reads_into_some_registers_from_memory() {
        let mut ms = MachineState::new();
        const SRC_ADDR: usize = 0x400;
        ms.ri = SRC_ADDR as u16;
        ms.memory[SRC_ADDR] = 1;
        ms.memory[SRC_ADDR + 1] = 2;
        ms.memory[SRC_ADDR + 2] = 4;
        ms.memory[SRC_ADDR + 3] = 8;
        evaluate(0xf365, &mut ms).unwrap();
        assert_eq!(1, ms.register(0));
        assert_eq!(2, ms.register(1));
        assert_eq!(4, ms.register(2));
        assert_eq!(8, ms.register(3));
        assert_eq!(0, ms.register(4));
    }

    #[test]
    fn increments_pc() {
        let mut ms = MachineState::new();
        let old_pc = ms.pc;
        evaluate(0xf165, &mut ms).unwrap();
        assert_eq!(old_pc + 2, ms.pc);
    }
}
