use crate::chip8;
use byte_tools::copy;
use byteorder::{BigEndian, ReadBytesExt};
use rand::prelude::*;
use std::fs::File;
use std::io::Cursor;
use std::io::{prelude::*, SeekFrom};
use std::num::Wrapping;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum RunningState {
    Running,
    WaitingForKeyPress(u8),
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum KeyState {
    Up,
    Down,
}

#[derive(Debug, PartialEq)]
pub struct MachineState {
    pub regs: [Wrapping<u8>; 16],
    pub ri: u16, // special register I
    pub delay_timer: u8,
    pub sound_timer: u8,
    pub pc: u16, // program counter
    pub stack: Vec<u16>,
    pub memory: Vec<u8>,
    pub code_size: usize,
    pub screen: Vec<u8>,
    pub keys: [KeyState; 16],
    pub running_state: RunningState,
}

impl MachineState {
    pub fn new() -> MachineState {
        let mut memory = vec![0; 4096];
        memory.splice(
            0..80,
            [
                0xf0, 0x90, 0x90, 0x90, 0xf0, // 0
                0x20, 0x60, 0x20, 0x20, 0x70, // 1
                0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
                0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
                0x90, 0x90, 0xF0, 0x10, 0x10, // 4
                0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
                0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
                0xF0, 0x10, 0x20, 0x40, 0x40, // 7
                0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
                0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
                0xF0, 0x90, 0xF0, 0x90, 0x90, // a
                0xE0, 0x90, 0xE0, 0x90, 0xE0, // b
                0xF0, 0x80, 0x80, 0x80, 0xF0, // c
                0xE0, 0x90, 0x90, 0x90, 0xE0, // d
                0xF0, 0x80, 0xF0, 0x80, 0xF0, // e
                0xF0, 0x80, 0xF0, 0x80, 0x80, // f
            ]
            .iter()
            .cloned(),
        );
        MachineState {
            regs: [Wrapping(0); 16],
            ri: 0,
            delay_timer: 0,
            sound_timer: 0,
            pc: chip8::PROGRAM_OFFSET,
            stack: Vec::new(),
            memory,
            code_size: 0,
            screen: vec![
                0;
                (chip8::SCREEN_WIDTH / 8 * chip8::SCREEN_HEIGHT)
                    as usize
            ],
            keys: [KeyState::Up; 16],
            running_state: RunningState::Running,
        }
    }

    #[allow(dead_code)]
    pub fn randomize_screen(&mut self) {
        let mut rng = rand::thread_rng();
        rng.fill_bytes(&mut self.screen);
    }

    pub fn load_program(&mut self, filename: &str) -> Result<(), String> {
        let mut f = File::open(filename).map_err(|e| e.to_string())?;
        let mut buf = Vec::new();
        f.read_to_end(&mut buf).map_err(|e| e.to_string())?;
        copy(&buf, &mut self.memory[0x200..]);
        self.code_size = buf.len();
        Ok(())
    }

    pub fn current_instruction(&self) -> std::io::Result<u16> {
        let mut cursor = Cursor::new(&self.memory);
        cursor.seek(SeekFrom::Start(self.pc as u64))?;
        Ok(cursor.read_u16::<BigEndian>().unwrap())
    }

    pub fn set_register(&mut self, i: usize, value: u8) {
        self.regs[i] = Wrapping(value);
    }

    pub fn register(&self, i: usize) -> u8 {
        self.regs[i].0
    }
}
