{...}: {
  perSystem = {
    pkgs,
    config,
    ...
  }: let
    crateName = "chip8";
  in {
    # declare projects
    nci.projects."simple".path = ./.;
    # configure crates
    nci.crates.${crateName} = {};
  };
}
